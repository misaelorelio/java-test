package com.app.estudo.teste.tdd.testetdd.repository;

import com.app.estudo.teste.tdd.testetdd.model.Livro;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface LivroRepository extends JpaRepository<Livro, Long> {

    Optional<Livro> findByReservas(String nome);
}
