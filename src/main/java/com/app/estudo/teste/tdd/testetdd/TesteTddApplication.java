package com.app.estudo.teste.tdd.testetdd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TesteTddApplication {

	public static void main(String[] args) {
		SpringApplication.run(TesteTddApplication.class, args);
	}

}
