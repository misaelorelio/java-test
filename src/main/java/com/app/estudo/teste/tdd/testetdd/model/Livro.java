package com.app.estudo.teste.tdd.testetdd.model;

import lombok.Data;

import java.time.LocalDate;

@Data
public class Livro {

    private Long id;
    private String reservas;
    private LocalDate checkIn;
    private LocalDate checkOut;

}
