package com.app.estudo.teste.tdd.testetdd.service;

import com.app.estudo.teste.tdd.testetdd.model.Livro;
import com.app.estudo.teste.tdd.testetdd.repository.LivroRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.Period;
import java.util.Optional;

@Service
public class LivroService {

    @Autowired
    LivroRepository livroRepository;
    public int calculoDiasComDataBase(String nome) {
        Optional<Livro> livroOptional = livroRepository.findByReservas(nome);
        return Period.between(livroOptional.get().getCheckIn(), livroOptional.get().getCheckOut()).getDays();
    }
}
