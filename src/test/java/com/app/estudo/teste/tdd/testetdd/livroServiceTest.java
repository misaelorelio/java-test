package com.app.estudo.teste.tdd.testetdd;

import com.app.estudo.teste.tdd.testetdd.model.Livro;
import com.app.estudo.teste.tdd.testetdd.repository.LivroRepository;
import com.app.estudo.teste.tdd.testetdd.service.LivroService;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.Optional;

@RunWith(SpringRunner.class)
public class livroServiceTest {

    @TestConfiguration
    static  class livroServiceConfiguracao {

        @Bean
        public LivroService livroService (){
            return new LivroService();
        }
    }
    @Autowired
    LivroService livroService;

    @MockBean
    LivroRepository livroRepository;

    @Test
    public void livroTesteCalculaDiasReserva() {
        String nome = "Misael";
        int dias = livroService.calculoDiasComDataBase(nome);

        Assertions.assertEquals(dias, 10);
    }

    @Before
    public void setup() {
        LocalDate checkIn = LocalDate.parse("2022-05-10");
        LocalDate checkOut = LocalDate.parse("2022-05-20");

        Livro livroModel = new Livro();
        livroModel.setCheckIn(checkIn);
        livroModel.setCheckOut(checkOut);
        livroModel.setId(1L);
        livroModel.setReservas("Misael");

        Mockito.when(livroRepository.findByReservas(livroModel.getReservas()))
                .thenReturn(Optional.of(livroModel));
    }
}
